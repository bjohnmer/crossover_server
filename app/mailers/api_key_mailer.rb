class ApiKeyMailer < ApplicationMailer
  default from: "bjohnmerprojectevaluation@gmail.com"

  def api_key_mail(api_key, url)
    @user = api_key
    @url = url
    mail(to: @user.email, subject: 'Here is your API Key')
  end
end
