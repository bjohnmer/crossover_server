class ApiController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_filter :restrict_access


  private
    def restrict_access

      authenticate_or_request_with_http_token do |token, options|
        ApiKey.where(access_token: token).exists?
      end
      
    end
end
