class WelcomeController < ApplicationController
  
  def new
    @api_key = ApiKey.new
  end

  # POST /
  def create
    @api_key = ApiKey.new(api_key_params)
    
    respond_to do |format|
      if @api_key.save
        
        url = request.original_url.gsub(/\s+/m, '/').strip.split("/")
        url[3] = url[2].gsub(/\s+/m, ':').strip.split(":")
        ApiKeyMailer.api_key_mail(@api_key, url).deliver

        format.html { redirect_to new_api_key_path, notice: 'Api Key has sent to your email.' }
        format.json { render :new, status: :created, location: root_path }
      else
        format.html { render :new }
        format.json { render json: @api_key.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    def api_key_params
      params.require(:api_key).permit(:email)
    end
end