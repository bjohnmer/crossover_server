class Api::V1::StatsController < ApiController
  
  respond_to :json

  before_action :cpu_load_data, only: [:cpu, :shutdown]

  def cpu
    cpu = [
      "user": @cpuData[0].to_f,
      "nice": @cpuData[1].to_f,
      "system": @cpuData[2].to_f,
      "iowait": @cpuData[3].to_f,
      "steal": @cpuData[4].to_f,
      "idle": @cpuData[5].to_f,
    ]
    
    cpu[0]["must_shutdown"] = true if @totalLoad >= 80 
    
    respond_with cpu.as_json
  end

  def disk
    txt = "df -h | grep /dev/xvda1"
    duc = `#{txt}`
    duData = duc.gsub(/\s+/m, ' ').strip.split(" ")
    disk = [
      "filesystem": duData[0],
      "size": duData[1].to_f,
      "used": duData[2].to_f,
      "available": duData[3].to_f,
      "used_percent": duData[4].to_f,
    ]
    respond_with disk.as_json

  end

  def processes
    
    usw = Usagewatch
    processes = [
      "cpu": usw.uw_cputop,
      "memory": usw.uw_memtop
    ]

    respond_with processes.as_json
  end

  def shutdown
    
    if @totalLoad >= 80 
      txt = "sudo shutdown -h now"
      duc = `#{txt}`
      respond_with shutdown = [
        "success": true
      ].as_json
    else
      respond_with shutdown = [
        "success": false
      ].as_json
    end

  end

  private
    def cpu_load_data
      txt = 'iostat -c | grep " 0."'
      duc = `#{txt}`
      @cpuData = duc.gsub(/\s+/m, '    ').strip.split("    ")
      @totalLoad = @cpuData[0].to_f + @cpuData[1].to_f + @cpuData[2].to_f + @cpuData[3].to_f + @cpuData[4].to_f 
    end

end
