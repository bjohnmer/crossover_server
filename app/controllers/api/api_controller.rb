class Api::ApiController < ApplicationController
  #respond_to :json
  
  def index
    respond_to do |format|
      format.json { render :json => {"status": 200}.as_json, status: :ok }
    end
    #respond_with status: 200
  end
end
