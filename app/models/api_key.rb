class ApiKey
  include Mongoid::Document
  include Mongoid::Timestamps

  field :access_token, type: String
  field :email, type: String

  before_create :generate_access_token
  
  validates_uniqueness_of :email, case_sensitive: false
  validates_presence_of :email
  validates :email, email: true

  private
    def generate_access_token
      begin
        self.access_token = SecureRandom.hex
      end while self.class.where(access_token: access_token).exists?
    end
end
