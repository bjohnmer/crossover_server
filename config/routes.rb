Rails.application.routes.draw do

  namespace :api, defaults: { format: 'json' } do
    scope module: :v1, constraints: ApiConstraints.new(version: 1, default: true) do
        get "cpu" => 'stats#cpu', :as => 'cpu'
        get "disk" => 'stats#disk', :as => 'disk'
        get "process" => 'stats#processes', :as => 'process'
        post "shutdown" => 'stats#shutdown', :as => 'shutdown'
    end
    get "/" => 'api#index', :as => 'api'
  end

  post "/" => 'welcome#create', :as => 'create_api_key'
  get "/" => 'welcome#new', :as => 'new_api_key'

  root 'welcome#new'
end
