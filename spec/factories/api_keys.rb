FactoryGirl.define do
    
  factory :api_key do |f|
    f.email { Faker::Internet.email }
  end
end
