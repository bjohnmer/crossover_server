# spec/controllers/welcome_controller_spec.rb
require 'spec_helper'
require 'rails_helper'
describe WelcomeController do
  describe "GET #new" do
    it "renders the :new view"  do
      get :new
      expect(subject).to render_template(:new)
    end
  end
  describe "POST #create" do
    context "with valid params" do
      it "creates a new ApiKey" do
        expect {
          post "/", :api_key => Factory.attributes_for(:api_key)
	  expect(response.body).to include("Api Key has send to your email.")
        }
      end
    end
  end
end
