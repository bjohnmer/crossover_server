# Preview all emails at http://localhost:3000/rails/mailers/api_key_mailer
class ApiKeyMailerPreview < ActionMailer::Preview
  def api_key_mail_preview
      ApiKyMailer.send_email(ApiKey.first, "ec2-52-26-99-224.us-west-2.compute.amazonaws.com")
  end
end
